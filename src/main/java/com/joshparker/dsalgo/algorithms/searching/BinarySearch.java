package com.joshparker.dsalgo.algorithms.searching;

public class BinarySearch {
    private BinarySearch() {}

    public static int search(int[] data, int target) {
        int min = 0;
        int max = data.length - 1;
        int middle;

        while(max >= min) {
            middle = (max + min) / 2;

            if (data[middle] == target) {
                return data[middle];
                // target to the right
            } else if (data[middle] > target) {
                max = middle - 1;
                // target to the left
            } else {
                min = middle + 1;
            }
        }
        return -1;
    }
}
