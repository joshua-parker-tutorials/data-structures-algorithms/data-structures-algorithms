package com.joshparker.dsalgo.algorithms.sorting;

/**
 * Worst Case Time Complexity [ Big-O ]: O(n2)
 *
 * Best Case Time Complexity [Big-omega]: O(n)
 *
 * Average Time Complexity [Big-theta]: O(n2)
 *
 * Space Complexity: O(1)
 */
public class InsertionSort {
    private InsertionSort() {}

    public static int[] sort(int[] data) {
        for (int i = 0; i < data.length; i++) {

            int j = i;
            while(j > 0 && data[j - 1] > data[j]) {
                int currValue = data[j];
                data[j] = data[j - 1];
                data[j - 1] = currValue;
                j--;
            }
        }
        return data;
    }
}
