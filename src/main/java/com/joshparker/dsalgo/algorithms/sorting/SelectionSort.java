package com.joshparker.dsalgo.algorithms.sorting;

/**
 * Worst Case Time Complexity [ Big-O ]: O(n2)
 *
 * Best Case Time Complexity [Big-omega]: O(n2)
 *
 * Average Time Complexity [Big-theta]: O(n2)
 *
 * Space Complexity: O(1)
 */
public class SelectionSort {
    private SelectionSort() {}

    public static int[] sort(int[] data) {
        for (int i = 0; i < data.length - 1; i++) {
            int currMinValueIndex = i;
            for (int j = i + 1; j < data.length; j++) {
                if (data[j] < data[currMinValueIndex]) {
                    currMinValueIndex = j;
                }
            }
            int smallerValue = data[currMinValueIndex];
            data[currMinValueIndex] = data[i];
            data[i] = smallerValue;
        }
        return data;
    }
}
