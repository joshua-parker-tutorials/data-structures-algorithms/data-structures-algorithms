package com.joshparker.dsalgo.algorithms.sorting;

/**
 * Following are the Time and Space complexity for the Bubble Sort algorithm.
 *
 * Worst Case Time Complexity [ Big-O ]: O(n2)
 * Best Case Time Complexity [Big-omega]: O(n)
 * Average Time Complexity [Big-theta]: O(n2)
 * Space Complexity: O(1)
 */
public class BubbleSort {
    private BubbleSort() {}

    public static int[] sort(int[] numbers) {
        for (int i = 0; i < numbers.length; i++) {
            int counter = 0;
            for (int j = 0; j < numbers.length - 1; j++) {
                if (numbers[j] > numbers[j + 1]) {
                    int current = numbers[j];
                    int next = numbers[j + 1];
                    numbers[j] = next;
                    numbers[j + 1] = current;
                    counter++;
                }
            }
            if (counter == 0)
                return numbers;
        }
        return numbers;
    }
}
