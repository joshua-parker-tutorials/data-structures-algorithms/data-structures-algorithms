package com.joshparker.dsalgo;

import com.joshparker.dsalgo.algorithms.searching.BinarySearch;
import com.joshparker.dsalgo.algorithms.sorting.BubbleSort;
import com.joshparker.dsalgo.algorithms.sorting.InsertionSort;
import com.joshparker.dsalgo.algorithms.sorting.SelectionSort;

import java.util.Arrays;

public class Main {
    private static final int[] ORDERED_NUMBERS   = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    private static final int[] UNORDERED_NUMBERS = {10, 8, 9, 2, 1, 5, 4, 3, 6, 7};

    public static void main(String... args) {
        System.out.println("\n*+*+*+*+*+*+*+*+*+*+ Algorithms *+*+*+*+*+*+*+*+*+*+\n");


        System.out.println("*** Searching ***");

        int value = BinarySearch.search(ORDERED_NUMBERS, 7);
        System.out.println("Binary Search: " + value + "\n");


        System.out.println("*** Sorting ***");

        int[] sortedBubbleNums = BubbleSort.sort(UNORDERED_NUMBERS);
        System.out.println("Bubble Sort: " + Arrays.toString(sortedBubbleNums));
        int[] insertionSortNums = InsertionSort.sort(UNORDERED_NUMBERS);
        System.out.println("Insertion Sort: " + Arrays.toString(insertionSortNums));
        int[] selectionSortNums = SelectionSort.sort(UNORDERED_NUMBERS);
        System.out.println("Selection Sort: " + Arrays.toString(selectionSortNums));
    }
}
