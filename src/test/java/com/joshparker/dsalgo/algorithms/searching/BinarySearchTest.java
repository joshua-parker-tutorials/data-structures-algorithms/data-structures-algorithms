package com.joshparker.dsalgo.algorithms.searching;

import org.junit.jupiter.api.*;

class BinarySearchTest {

    @Test
    void givenArrayOfIntegersAndTarget_inOrderOfLowestToHighestAndTargetIsIncluded_returnsSpecifiedTarget() {
        int[] numbers = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        int expectedValue = 2;

        int responseValue = BinarySearch.search(numbers, expectedValue);

        Assertions.assertEquals(expectedValue, responseValue);
    }
}