package com.joshparker.dsalgo.algorithms.sorting;

import org.junit.jupiter.api.*;

class BubbleSortTest {

    @Test
    void givenArrayOfIntegers_notInOrderOfLowestToHighest_returnsArrayInOrderOfLowestToHighest() {
        int[] array = {10,8,9,2,1,5,4,3,6,7};
        int[] expectedArray = {1,2,3,4,5,6,7,8,9,10};
        int[] arrayWithDuplicates = {3,6,3,8,9,32,89,34,67,12,333,567,4,3,457,90};
        int[] expectedArrayWithDuplicates = {3,3,3,4,6,8,9,12,32,34,67,89,90,333,457,567};
        int[] arrayWithNegativeNumbers = {1,4,-1,2,-7,0,6,8,-23};
        int[] expectedArrayWithNegativeNumbers = {-23,-7,-1,0,1,2,4,6,8};

        int[] responseArray = BubbleSort.sort(array);
        int[] responseArrayDuplicates = BubbleSort.sort(arrayWithDuplicates);
        int[] responseArrayNegatives = BubbleSort.sort(arrayWithNegativeNumbers);

        Assertions.assertArrayEquals(expectedArray, responseArray);
        Assertions.assertArrayEquals(expectedArrayWithDuplicates, responseArrayDuplicates);
        Assertions.assertArrayEquals(expectedArrayWithNegativeNumbers, responseArrayNegatives);
    }

    @Test
    void givenArrayOfIntegers_inOrderOfLowestToHighest_returnsArrayInOrderOfLowestToHighest() {
        int[] numbersInOrder = {1,2,3,4,5,6,7,8,9,10};
        int[] expectedValue = {1,2,3,4,5,6,7,8,9,10};

        int[] responseValue = BubbleSort.sort(numbersInOrder);

        Assertions.assertArrayEquals(expectedValue, responseValue);
    }

    @Test
    void givenEmptyArray_returnsEmptyArray() {
        int[] numbers = {};
        int[] expectedValue = {};

        int[] responseValue = BubbleSort.sort(numbers);

        Assertions.assertArrayEquals(expectedValue, responseValue);
    }

}